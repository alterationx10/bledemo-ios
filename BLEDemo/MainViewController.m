//
//  MainViewController.m
//  BLEDemo
//
//  Created by Mark Rudolph on 7/21/14.
//  Copyright (c) 2014 Mark's Laboratory. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

// Synthesize all of our properties
@synthesize centralManager;
@synthesize blePeripheral;
@synthesize scanActivityIndicator;
@synthesize scannedCBPeripherals;
@synthesize scannedDeviceTable;
@synthesize isScanning;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// This initializer is called when a Storyboard is handling things.
// Just a general FYI, since starting with an Empty Project leaves this out by default.
-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Set up our objects
    
    // You can use your own queue, but easiest to jsut use the default one:
    // From the Documentation (queue):
    // The dispatch queue to use to dispatch the central role events. If the value is nil, the central manager dispatches central role events using the main queue.
    // So you can call dispatch_get_main_queue(), or jsut use nil, and it should provide the same result.
    centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    
    scannedCBPeripherals = [[NSMutableArray alloc] init];
    
    blePeripheral = nil;
    
    // We want to be the delegate and source of data for our table.
    // This is specified in the Storyboard, but you could do it here as well...
    //[scannedDeviceTable setDelegate:self];
    //[scannedDeviceTable setDataSource:self];
    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // We want to make sure we're the delegate whenever this viewcontroller appears!
    // Not really needed for simple apps, but if you pass around the reference to the object to multiple view controllers,
    // you don't want to "forget" who the delegate is.
    if ([centralManager delegate] != self) {
        [centralManager setDelegate:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// **************************************************************
//
// Storyboard Actions
//
// **************************************************************

- (IBAction)scanButtonPressed:(id)sender {
    // Nothing worse than users who push buttons repeatedly ;-)
    if (!isScanning) {
        // Clear any old scans out of out table / array
        [scannedCBPeripherals removeAllObjects];
        [scannedDeviceTable reloadData];
        [self startScanning];
    }
}

// **************************************************************
//
// Our useful functions
//
// **************************************************************


-(void)startScanning {
    // Tell the CBCentralManager to start scanning for *ANY* BLE device
    // If you only want to scan for devices taht contain certain service,
    // then provice an NSArray of CBUUIDs of the Services you are looking for
    // You can provide an NSDictionary of some scanning options as well (see Documentation)
    [centralManager scanForPeripheralsWithServices:nil options:nil];
    
    // Make our activity indicator spin, so we know something is happening
    // It is set to hide when not animating via the Storyboard interface.
    [scanActivityIndicator startAnimating];
    
    // Use an NSTimer to stop scanning after a designated time.
    // Once you start scanning, it won't stop until you tell it to!
    // We will stop it after 10 seconds using our stopScanning function
    [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanning) userInfo:nil repeats:NO];
    
    // Set our BOOL to true
    // We mainly don't want people mashing the button and creating extra NSTImers to cancel the scan.
    // If we call centralManger to scan again, and it is still scanning, it should be continue, but jsut update any scan settings passed
    [self setIsScanning:YES];
}

-(void)stopScanning {
    // Stop the scanning
    [centralManager stopScan];
    
    // Stop our activity indicator
    [scanActivityIndicator stopAnimating];
    
    // Set our BOOL flag to false
    [self setIsScanning:NO];
    
}

// **************************************************************
//
// Our CBCentralManager methods
//
// **************************************************************

// This one is required, but the rest are optional. You only need to implement the ones you need/use.
// Here is where you get notifications like wheater BLuetooth on the iOS device is powered on / off (or changes!)
-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"Bluetooth is off!");
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"Bluetooth is on");
    }
    else {
        // parse through other states of interest...
    }
    
}

// This is fired whever we have discoverd a BLE device during our scan!
-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    // Get our devices friendly name
    NSString *deviceName = [peripheral name];
    // BLE is sort of a  "packet burst" technology, and sometime this information is "sniffed" and not complete on the first scan (but will get there eventually).
    // Make sure our deviceName isn't null.
    if (deviceName == nil) {
        deviceName = @"Unknown Name!";
    }
    
    NSLog(@"Just found a device: %@", deviceName);
    
    // Add the device to our array
    [scannedCBPeripherals addObject:peripheral];
    
    // Don't try and do anything directly with the peripheral object of this function, as it will be deallocated very soon.
    // Do something like this if you think you've found the one you want to interact with
    // [self setBlePeripheral:peripheral];
    // [blePeripheral doWhatEver...];
    
    // Reload our table with the newest information
    [scannedDeviceTable reloadData];
}

// This fires when you successfully connect to a BLE device
-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    
    NSString *deviceName = [peripheral name];
    if (deviceName == nil) {
        deviceName = @"Unknown Name!";
    }
    NSLog(@"Just connected to device: %@", deviceName);
    
    // Now would be a good time to set our blePeripheral
    [self setBlePeripheral:peripheral];
    
    // Before we can use it's service/characteristics, we need to discover them first!
    // Since we've connected to it, now would be a good time to do that.
    // We have to kick off a "daisy chain" of events.
    // First we need to discover the avaialble serivces (callback: didDiscoverServices),
    // and then once we've found those, we need to find the characteristics of that service callback: didDiscoverCharacteristicsForService)
    // Note: if you are only interested in characteristics of a particualr servie, provide an array of those CBUUIDs.
    // nil discovers them all.
    [blePeripheral discoverServices:nil];

}

// **************************************************************
//
// Our CBPeripheral methods
//
// **************************************************************

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    // Log just in case we got an error
    if (error)
    {
        NSLog(@"didDiscoverCharacteristicsForService with error: %@", [error localizedDescription]);
        return;
    }
    // Discover all the characteristics in the service
    for (CBService *service in peripheral.services ) {
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    // Here is where we ought to load our characteristics into a diction, or soemthing.
    // Example coming soon.
}

// **************************************************************
//
// Our UITable methods for udating the UI
//
// **************************************************************

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // The table should only be as long as the number of elements in our array
    return [scannedCBPeripherals count];
}

// Configure each cell of the table
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    // Make the cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:CellIdentifier];
    }
    
    // Get a specific peripheral from the list
    CBPeripheral *aPeripheral = [scannedCBPeripherals objectAtIndex:[indexPath row]];
    NSString *name = [aPeripheral name];
    // I don't think name should be nil by now, but it never hurts to be sure
    if (name == nil || [name isEqualToString:@""]) {
        name = @"Unknown device name!";
    }
    // Configure the cell
    // Cells have an image view, so you can set a neat little icon here pretty easily!
    // I don't have any neat icons handy, so I'll use a lame one.
    cell.imageView.image = [UIImage imageNamed:@"lame_icon.png"];
    // Set the text of the cell to be the device name
    cell.textLabel.text = name;
    return cell;
}

// You should implement this to properly return wich index from the table you are selecting.
// This is handy is you need to do an offset (i.e. have a placeholder row, or soemthing)
// We don't need to modify anything, so jsut pass the original value
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

// This gets fired when you select an item from the table.
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // The indexPath should match our array of scanned objects, so we can call one from our array
    CBPeripheral *aPeripheral = [scannedCBPeripherals objectAtIndex:[indexPath row]];
    // Let's show something other than the name
    NSString *deviceUUID = [[aPeripheral identifier] UUIDString];

    
    // It would be handy to connect to a device right here!
    // More to come later
    // For now, jsut display a UIAlert
    [[[UIAlertView alloc] initWithTitle:@"You clicked something!" message:[NSString stringWithFormat:@"You just selected a device with identifer: %@", deviceUUID] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    
    // We are not a UITableVIewController, so we have to manually unselect a selection
    [scannedDeviceTable deselectRowAtIndexPath:indexPath animated:YES];

}
@end
