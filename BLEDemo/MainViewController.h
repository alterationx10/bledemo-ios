//
//  MainViewController.h
//  BLEDemo
//
//  Created by Mark Rudolph on 7/21/14.
//  Copyright (c) 2014 Mark's Laboratory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

// CBCentralManagerDelegate
// We want to be a delegate of CBCentralManager so we can be notified about connections/scans/typical BLE stuff

// CBPeripheralDelegate
// We want to be a delegate of CBPeripheral so we can interact with a BLE device

// UITableViewDataSource
// We are going to show a table of found devices; indicate that this is the source for the data

// UITableViewDelegate
// We want to be the delegate for the Table, so we can do things when an item is clicked.

@interface MainViewController : UIViewController <CBCentralManagerDelegate, CBPeripheralDelegate, UITableViewDataSource, UITableViewDelegate>

// We need a CBCentralManager to scan for / connect to BLE devices
@property CBCentralManager *centralManager;

// We need a CBPeripheral that we can connect to / interact with
@property CBPeripheral *blePeripheral;

// For scanning devices, we will use a NSMutableArray to hold the results for showing in our table
@property NSMutableArray *scannedCBPeripherals;

// A BOOL for us to use so we know if we are currently scanning or not.
@property BOOL isScanning;

//
// Some useful functions
//

// Call to start scanning for BLE devices
-(void)startScanning;
// Call to stop scanning for BLE devices
-(void)stopScanning;

//
// Things from our Storyboard
//

// We will display our results in this table from our Storyboard
@property (weak, nonatomic) IBOutlet UITableView *scannedDeviceTable;

// A little activity indidcator to show whether we are actively scanning or not
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *scanActivityIndicator;

// A button action to start the scanning procedure
- (IBAction)scanButtonPressed:(id)sender;

@end
